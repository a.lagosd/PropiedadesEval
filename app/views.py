from django.shortcuts import render,get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.utils import timezone
from django.contrib.auth import authenticate, login, logout
from django.contrib import messages
from .forms import RegUsr, LoginForm
from .models import List
from django.core.mail import send_mail
from django.contrib.auth.forms import (
    AuthenticationForm, PasswordChangeForm, PasswordResetForm, SetPasswordForm,
)

# Create your views here.
def home(request):
    return render (request, 'app/home.html')
def galeriaImagenes(request):
    return render(request, 'app/galeriaImagenes.html')

def consulta(request):
    return render(request, 'app/consulta.html')

#INICIAR SESIÓN
def cuenta(request):
    return render(request, 'app/cuenta.html')

#REGISTRARSE
def registro(request):
    form1 = RegUsr
    if request.method == "POST":
        form1 = RegUsr(request.POST)

        if form1.is_valid():
            form1.save()
            messages.success(request,'Se ha registrado con exito!')
        return redirect("cuenta.html")
    else:
        form1 = RegUsr()
    return render(request, "app/registro.html", {"form1":form1})

