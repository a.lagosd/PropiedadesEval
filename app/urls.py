from django.urls import path, include
from .views import home, consulta, galeriaImagenes, registro,cuenta

urlpatterns = [
    path('', home, name="home"),
    path('galeriaImagenes/', galeriaImagenes, name="galeriaImagenes"),
    path('consulta/', consulta, name="consulta"),
    path('registro/', registro, name="registro"),
    # path('cuenta/', include('django.contrib.auth.urls')),
    path('cuenta/', cuenta, name="cuenta"),
]
